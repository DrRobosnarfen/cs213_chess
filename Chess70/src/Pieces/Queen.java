package Pieces;

import Chess.Gameboard;
import Chess.Tile;

/**
 * class representation of a queen, that extends the superclass 'Piece'
 *
 */
public class Queen extends Piece{
    
    /**
     * @param colour
     * initializes a new Queen object with a colour assignment
     */
    public Queen(boolean colour){
        super(colour);
    }

    /* (non-Javadoc)
     * @see Pieces.Piece#getChessPiece()
     * assigns to black or white team
     */
    @Override
    public String getChessPiece() {
        if(isWhite)
            return "wQ";
        else
            return "bQ";
    }

    /* (non-Javadoc)
     * @see Pieces.Piece#movePiece(Game.Tile, Game.Tile, Game.Gameboard, boolean)
     * queen's moves
     */
    @Override
    public boolean movePiece(Tile start, Tile dest, Gameboard gb, boolean real){
        if(!super.movePiece(start, dest, gb, real))
            return false;
        int dx = dest.getX();
        int dy = dest.getY();
        //Illegal move: not diagonal, horizontal, or vertical
        if(!checkCardinal(dx, dy, gb) && !checkDiagonal(dx, dy, gb))
            return false;
        if(real) {
            Piece enemyKing = gb.whiteKing;
            Piece myKing = gb.blackKing;
            if (isWhite) {
                enemyKing = gb.blackKing;
                myKing = gb.whiteKing;
            }
            start.updateTile(null);
            if(gb.threatenedTile(gb.getTile(myKing), !isWhite)){
                start.updateTile(this);
                return false;
            }
            if (dest.getPiece() != null){
                decrementPieces(dest.getPiece(), gb);
                if(dest.getPiece()==gb.checkingWhite[0]){
                    gb.checkingWhite[0] = null;
                } else if(dest.getPiece()==gb.checkingBlack[0]){
                    gb.checkingBlack[0] = null;
                }
            }
            set_xy(dx, dy);
            dest.updateTile(this);
            if(gb.threatenedTile(gb.getTile(enemyKing), isWhite)){
                gb.setCheck(isWhite);
            }
        }
        return true;
    }
}
