package Pieces;

import Chess.Gameboard;
import Chess.Tile;

public abstract class Piece {
    public boolean isWhite;
    public boolean isKnight = false;
    protected boolean hasMoved = false;
    public int x, y;

    public abstract String getChessPiece();
    public Piece(boolean colour) {
        isWhite = colour;
    }
    public void set_xy(int x, int y) {
        this.x = x;
        this.y = y;
    }
    public boolean movePiece(Tile start, Tile dest, Gameboard gb, boolean realMove){
//        gb.debug_move_string("", "", start.getPiece(), dest.getPiece());
        //Illegal move: trying to move to it's current location
        if (x == dest.getX() && y == dest.getY())
            return false;
        //Illegal move: trying to capture an ally
        if (dest.getPiece() != null) {
            if (dest.getPiece().isWhite == isWhite && !gb.COUNTERATTACK_FLAG)
                return false;
        }
        return true;
    }
    public void decrementPieces(Piece occupant, Gameboard gb){
        if(occupant.isWhite)
            gb.remainingWhite--;
        else
            gb.remainingBlack--;
    }
    public Boolean checkCardinal(int dx, int dy, Gameboard gb){
        //Illegal move: not moving in a horizontal or vertical line
        if(x!=dx && y!=dy)
            return false;
        int i;
        //if it's a vertical move (x coordinate stays the same, but not y)
        if(x==dx && y!=dy){
            if(dy>y) {
                //if dy larger than current y, check the locations in between dy and y by decrementing from dy
                i = dy;
                while(--i!=y){
                    if(gb.getTile(dx, i).getPiece()!= null)
                        return false;
                }
            }
            else{ //(dy<y)
                //if dy is smaller than current y, check the locations in between dy and y by incrementing from y
                i = dy;
                while(++i != y){
                    if(gb.getTile(dx, i).getPiece()!=null)
                        return false;
                }
            }
        }
        else{
            if(dx>x){
                i = dx;
                while(--i!=x){
                    if(gb.getTile(i, dy).getPiece()!=null) {
                        return false;
                    }
                }
            }
            else{ //(dx<x)
                i = dx;
                while(++i!=x){
                    if(gb.getTile(i, dy).getPiece()!=null)
                        return false;
                }
            }
        }
        return true;
    }
    public Boolean checkDiagonal(int dx, int dy, Gameboard gb){
        //Illegal move: not moving in a diagonal line
        if(Math.abs(dx-x) != Math.abs(dy-y))
            return false;
        int i = 0;
        if(x>dx){
            if(y>dy) {
                //starting point is greater than destination on both axis (diagonal movement southwest)
                while(y-++i !=dy){
                    if(gb.getTile(x-i, y-i).getPiece()!= null)
                        return false;
                }
            }
            else{ //(y<dy)
                i = 0;
                //starting point is greater on x axis, less on y axis (diagonal movement northwest)
                while(dy-++i !=y){
                    if(gb.getTile(x-i, y+i).getPiece()!=null)
                        return false;
                }
            }
        }
        else{ //(x<dx)
            if(y>dy) {
                i = 0;
                //starting point is less on x axis, greater on y axis (diagonal movement southeast)
                while(y-++i !=dy){
                    if(gb.getTile(x+i, y-i).getPiece()!= null)
                        return false;
                }
            }
            else{ //(y<dy)
                i = 0;
                //starting point is less on both axis (diagonal movement northeast)
                while(dy-++i !=y){
                    if(gb.getTile(x+i, y+i).getPiece()!=null)
                        return false;
                }
            }
        }
        return true;
    }
}