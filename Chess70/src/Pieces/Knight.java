package Pieces;

import Chess.Gameboard;
import Chess.Tile;

/**
 * @class representation of a knight, that extends the superclass 'Piece'
 *
 */
public class Knight extends Piece{
    /**
     * @param colour
     * initializes a new Knight object with a colour assignment
     */
    public boolean isKnight;

    public Knight(boolean colour) {
        super(colour);
        isKnight = true;
    }

    /* (non-Javadoc)
     * @see Pieces.Piece#getChessPiece()
     * assigns to black or white team
     */
    @Override
    public String getChessPiece() {
        if (isWhite)
            return "wN";
        else
            return "bN";
    }

    /* (non-Javadoc)
     * @see Pieces.Piece#movePiece(Game.Tile, Game.Tile, Game.Gameboard, boolean)
     * knights moves
     */

    @Override
    public boolean movePiece(Tile start, Tile dest, Gameboard gb, boolean real){
        if(!super.movePiece(start, dest, gb, real))
            return false;
        int dx = dest.getX();
        int dy = dest.getY();
        if (!(Math.abs(dx - x) == 1 && Math.abs(dy - y) == 2) && !(Math.abs(dx - x) == 2 && Math.abs(dy - y) == 1))
            return false;
        if(real) {
            Piece enemyKing = gb.whiteKing;
            Piece myKing = gb.blackKing;
            if (isWhite) {
                enemyKing = gb.blackKing;
                myKing = gb.whiteKing;
            }
            start.updateTile(null);
            if(gb.threatenedTile(gb.getTile(myKing), !isWhite)){
                start.updateTile(this);
                return false;
            }
            if (dest.getPiece() != null){
                decrementPieces(dest.getPiece(), gb);
                if(dest.getPiece()==gb.checkingWhite[0]){
                    gb.checkingWhite[0] = null;
                } else if(dest.getPiece()==gb.checkingBlack[0]){
                    gb.checkingBlack[0] = null;
                }
            }
            set_xy(dx, dy);
            dest.updateTile(this);
            if(gb.threatenedTile(gb.getTile(enemyKing), isWhite)){
                gb.setCheck(isWhite);
            }
        }
        return true;
    }
}
