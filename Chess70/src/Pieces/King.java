package Pieces;

import Game.Gameboard;
import Game.Tile;

/**
 * class representation of a king, that extends the superclass 'Piece'
 *
 */
public class King extends Piece {
    /**
     * @param colour
     * initializes a new King object with a colour assignment
     */
    public King(boolean colour){
        super(colour);
    }

    /* (non-Javadoc)
     * @see Pieces.Piece#getChessPiece()
     * assigns to white or black team
     */
    @Override
    public String getChessPiece() {
        if(isWhite)
            return "wK";
        else
            return "bK";
    }

    /* (non-Javadoc)
     * @see Pieces.Piece#movePiece(Game.Tile, Game.Tile, Game.Gameboard, boolean)
     * King's moves
     */
    @Override
    public boolean movePiece(Tile start, Tile dest, Gameboard gb, boolean real){
        if(!super.movePiece(start, dest, gb, real))
            return false;
        int dx = dest.getX();   //destination x
        int dy = dest.getY();   //destination y
        if(Math.abs(gb.blackKing.x-gb.whiteKing.x)==1 && Math.abs(gb.blackKing.y-gb.whiteKing.y)==1)
            return false;
        if(gb.threatenedTile(dest, !isWhite))
            return false;
        if(Math.abs(x-dx)>1 || Math.abs(y-dy)>1) {
            if(Math.abs(x-dx)==2){
                if(hasMoved || (isWhite&&gb.whiteCheck) || (!isWhite&&gb.blackCheck))
                    return false;
                if(dx==6){
                    if(gb.getTile(5, dy).getPiece()!= null || gb.getTile(6, dy).getPiece()!=null || gb.getTile(7, dy).getPiece()==null)
                        return false;
                    else if(gb.getTile(7,dy).getPieceString().charAt(1)!='R' || gb.getTile(7,dy).getPiece().hasMoved)
                        return false;
                    if(gb.threatenedTile(gb.getTile(5,dy), !isWhite) || gb.threatenedTile(gb.getTile(6, dy), !isWhite))
                        return false;
                    gb.getTile(7,dy).getPiece().movePiece(gb.getTile(7,dy), gb.getTile(5,dy), gb, true);
                } else if(dx==2){
                    if(gb.getTile(3, dy).getPiece()!= null || gb.getTile(2, dy).getPiece()!=null || gb.getTile(0, dy).getPiece()==null)
                        return false;
                    else if(gb.getTile(0,dy).getPieceString().charAt(1)!='R' || gb.getTile(0,dy).getPiece().hasMoved)
                        return false;
                    if(gb.threatenedTile(gb.getTile(3, dy), !isWhite) || gb.threatenedTile(gb.getTile(2, dy), !isWhite))
                        return false;
                    gb.getTile(0,dy).getPiece().movePiece(gb.getTile(0,dy), gb.getTile(3,dy), gb, true);
                }
            } else
                return false;
        }
        if(real) {
            if(dest.getPiece()!=null)
                decrementPieces(dest.getPiece(), gb);
            set_xy(dest.getX(), dest.getY());
            dest.updateTile(this);
            start.updateTile(null);
            hasMoved = true;
            if (gb.blackCheck){
                gb.blackCheck = false;
                gb.checkingBlack[0] = null;
                gb.checkingBlack[1] = null;
            }
            if (gb.whiteCheck){
                gb.whiteCheck = false;
                gb.checkingWhite[0] = null;
                gb.checkingWhite[1] = null;
            }

        }
        return true;
    }
}
