package Pieces;

import Game.Gameboard;
import Game.Tile;

/**
 * class representation of a rook, that extends the superclass 'Piece'
 *
 */
public class Rook extends Piece{
    
    /**
     * @param colour
     * initializes a new Rook object with a colour assignment
     */
    public Rook(boolean colour) {
        super(colour);
    }

    /* (non-Javadoc)
     * @see Pieces.Piece#getChessPiece()
     * assigns to black or white team
     */
    @Override
    public String getChessPiece() {
        if (isWhite)
            return "wR";
        else
            return "bR";
    }
    
    /* (non-Javadoc)
     * @see Pieces.Piece#movePiece(Game.Tile, Game.Tile, Game.Gameboard, boolean)
     * rook's moves
     */

    @Override
    public boolean movePiece(Tile start, Tile dest, Gameboard gb, boolean real){
        if(!super.movePiece(start, dest, gb, real))
            return false;
        //Illegal move: not moving in a horizontal or vertical line
        if(!checkCardinal(dest.getX(), dest.getY(), gb))
            return false;
        if(real) {
            Piece enemyKing = gb.whiteKing;
            Piece myKing = gb.blackKing;
            if (isWhite) {
                enemyKing = gb.blackKing;
                myKing = gb.whiteKing;
            }
            start.updateTile(null);
            if(gb.threatenedTile(gb.getTile(myKing), !isWhite)){
                start.updateTile(this);
                return false;
            }
            if (dest.getPiece() != null){
                decrementPieces(dest.getPiece(), gb);
                if(dest.getPiece()==gb.checkingWhite[0]){
                    gb.checkingWhite[0] = null;
                } else if(dest.getPiece()==gb.checkingBlack[0]){
                    gb.checkingBlack[0] = null;
                }
            }
            set_xy(dest.getX(), dest.getY());
            dest.updateTile(this);
            hasMoved = true;
            if(gb.threatenedTile(gb.getTile(enemyKing), isWhite)){
                gb.setCheck(isWhite);
            }
        }

        return true;
    }
}
