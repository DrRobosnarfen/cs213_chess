package Chess;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Chess {

    public static void main(String[] args) throws IOException {
        Gameboard game = new Gameboard();
        boolean legal_move;
        while(game.inProgress){
            game.parseInput("e2 e4");
            game.parseInput("e7 e5");
            game.parseInput("d1 f3");
            game.parseInput("a7 a6");
            game.parseInput("f1 c4");
            game.parseInput("a6 a5");
            game.printBoard();
            do {
                legal_move = true;
                BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
                legal_move = game.parseInput(in.readLine());
                if(!legal_move)
                    System.out.println("Illegal move, try again");
            }while(!legal_move);
            if(game.inProgress)
                System.out.println();
            else
                game.printBoard();
        }

    }
}
