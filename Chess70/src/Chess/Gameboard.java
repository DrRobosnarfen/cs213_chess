package Chess;

import Pieces.*;
import java.lang.*;
import java.util.StringTokenizer;

public class Gameboard {
    private final boolean DEBUG = false;
    public static boolean COUNTERATTACK_FLAG = false;
    public static boolean whiteCheck, blackCheck;
    public static Piece whiteKing = new King(true), blackKing = new King(false);
    public static boolean whiteKing_immobile = true, blackKing_immobile = true;
    public static int remainingWhite = 16, remainingBlack = 16;
    private Tile[][] board = new Tile[8][8];
    public static Tile EP_Pawn;
    public Piece[] checkingWhite = new Piece[2];
    public Piece[] checkingBlack = new Piece[2];
    public static int EN_PASSANT_FLAG = 0;
    public boolean whiteTurn = true, inProgress = true;
    private boolean DRAW_FLAG = false;
    public static char Promotion_class = 'Q';
    //New board setup
    public Gameboard(){
        for (int i = 0; i < 8; i++) {
            for(int j=6; j>=1; j--) {
                int colour = (i+j)%2;
                if(j==6)
                    board[i][j] = new Tile(colour, i, j, new Pawn(false));
                else if (j==1)
                    board[i][j] = new Tile(colour, i, j, new Pawn(true));
                else
                    board[i][j] = new Tile(colour, i, j);
            }
        }
        board[0][7] = new Tile(1, 0, 7, new Rook(false));
        board[1][7] = new Tile(0, 1, 7, new Knight(false));
        board[2][7] = new Tile(1, 2, 7, new Bishop(false));
        board[3][7] = new Tile(0, 3, 7, new Queen(false));
        board[4][7] = new Tile(1, 4, 7, blackKing);
        board[5][7] = new Tile(0, 5, 7, new Bishop(false));
        board[6][7] = new Tile(1, 6, 7, new Knight(false));
        board[7][7] = new Tile(0, 7, 7, new Rook(false));
        board[0][0] = new Tile(0, 0, 0, new Rook(true));
        board[1][0] = new Tile(1, 1, 0, new Knight(true));
        board[2][0] = new Tile(0, 2, 0, new Bishop(true));
        board[3][0] = new Tile(1, 3, 0, new Queen(true));
        board[4][0] = new Tile(0, 4, 0, whiteKing);
        board[5][0] = new Tile(1, 5, 0, new Bishop(true));
        board[6][0] = new Tile(0, 6, 0, new Knight(true));
        board[7][0] = new Tile(1, 7, 0, new Rook(true));
    }
    //prints the current board state
    public void printBoard(){
        for(int j=7; j>=0; j--) {
            for(int i=0; i<8; i++){
                System.out.print(board[i][j].getPieceString() + " ");
            }
            System.out.print(j+1);
            System.out.println();
        }
        for(int i=0; i<8; i++){
            int j = 97+i;
            System.out.print(" " + (char)j + " ");
        }
        System.out.println();
        System.out.println();
        if(whiteTurn)
            System.out.print("White's move: ");
        else
            System.out.print("Black's move: ");
    }
    public Tile getTile(int x, int y){
        return board[x][y];
    }
    public Tile getTile(Piece p){
        return board[p.x][p.y];
    }
    public boolean parseInput(String s){
        //empty line.
        if(s.isEmpty()){
            return false;
        }
        StringTokenizer tokenizer = new StringTokenizer(s," ");
        String[] text = new String[3];
        int inputCount= 0;
        while(inputCount<3 && tokenizer.hasMoreTokens()) {
            text[inputCount++] = tokenizer.nextToken();
        }
        //if only 1 input, must be "draw" or "resign", else invalid
        if(inputCount==1){
            if(text[0].equals("draw")) {
                if (DRAW_FLAG) {
                    inProgress = false;
                    System.out.print("draw");
                    return true;
                }
            }
            if(text[0].equals("resign")){
                inProgress = false;
                if(whiteTurn)
                    System.out.print("Black wins");
                else
                    System.out.print("White wins");
                return true;
            }
            return false;
        }
        DRAW_FLAG = false;
        //if 3 inputs, 3rd input must be "draw?" or promotion specifications, else invalid.
        if(inputCount==3) {
            if (text[2].equals("draw?"))
                DRAW_FLAG = true;
            else if (text[2].length() == 1 && Character.isLetter(text[2].charAt(0))) {
                char temp = Character.toUpperCase(text[2].charAt(0));
                if (temp == 82 || temp == 81 || temp == 78 || temp == 66) {
                    Promotion_class = temp;
                } else
                    return false;
            } else
                return false;
        }
        //if must be 2 or 3 inputs, first two inputs are chess fileRanks. Checks that fileRank input is the correct format.
        if(text[0].length()==2 && Character.isLetter(text[0].charAt(0)) && Character.isDigit(text[0].charAt(1)) &&
            text[1].length()==2 && Character.isLetter(text[1].charAt(0)) && Character.isDigit(text[1].charAt(1)))
            return parseCoordinates(text[0], text[1]);
        return false;
    }
    public boolean parseCoordinates(String start, String dest){
        //start_file, start_rank, dest_file, dest_rank
        int sx = start.charAt(0) - 97;
        int sy = start.charAt(1) - '0' - 1;
        int dx = dest.charAt(0) - 97;
        int dy = dest.charAt(1) - '0' - 1;
        if (sx>7 || sy>7 || dx>7 || dy>7 || sx<0 || sy<0 || dx<0 || dy<0) {
            return false;
        }
        Piece startPiece = board[sx][sy].getPiece();
        if(blackCheck && startPiece!=blackKing)
            return false;
        if(whiteCheck && startPiece!=whiteKing)
            return false;
        //attempting to move an empty tile
        if(startPiece==null){
            return false;
        }
        //attempting to move the other player's piece
        if((startPiece.isWhite && !whiteTurn) || (!startPiece.isWhite && whiteTurn)){
            return false;
        }
        if(DEBUG){
            System.out.println("array index start: board[" + sx + "][" + (sy+1) + "]");
            System.out.println("array index destination: board[" + dx+ "][" + (dy+1) + "]");
            debug_move_string(start, dest, startPiece, board[dx][dy].getPiece());
        }
        boolean legal_move = startPiece.movePiece(board[sx][sy], board[dx][dy], this, true);
        if(legal_move)
            whiteTurn = !whiteTurn;
        determineMobility(whiteTurn);
        if(blackCheck && blackKing_immobile){
            System.out.print(checkingBlack[0].getChessPiece());
            if(!hasSavior(false)){
                System.out.print("Checkmate");
                System.out.print("White wins");
                inProgress = false;
            }
        }
        if(whiteCheck && whiteKing_immobile){
            if(!hasSavior(true)){
                System.out.println("Checkmate");
                System.out.print("Black wins");
                inProgress = false;
            }
        }
        if(EN_PASSANT_FLAG>0)
            EN_PASSANT_FLAG--;
        if(DEBUG){
            System.out.println("b Check, Immobile? " + blackCheck + ", " + blackKing_immobile);
            System.out.println("w Check,  Immobile? " + whiteCheck + ", " + whiteKing_immobile);
            System.out.println("remaing white piece: " + remainingWhite + " black: " + remainingBlack);
        }
        return legal_move;
    }
    public boolean hasSavior(boolean colour){
        Piece attacker;
        int i, j;
        if(colour){
            if(checkingWhite[0]!=null && checkingWhite[1]!=null)
                return false;
            attacker = checkingWhite[0];
            if(attacker.isKnight)
                return threatenedTile(getTile(attacker), colour);
            if(attacker.x >= whiteKing.x){
                for(i=attacker.x; i>whiteKing.x; i--) {
                    if (attacker.y >= whiteKing.y) {
                        for (j = attacker.y; j > whiteKing.y; j--) {
                            if (threatenedTile(getTile(i, j), colour))
                                return true;
                        }
                    } else {
                        for (j = attacker.y; j < whiteKing.y; j++) {
                            if (threatenedTile(getTile(i, j), colour))
                                return true;
                        }
                    }
                }
            } else {
                for(i=attacker.x; i<whiteKing.x; i++) {
                    if (attacker.y >= whiteKing.y) {
                        for (j = attacker.y; j > whiteKing.y; j--) {
                            if (threatenedTile(getTile(i, j), colour))
                                return true;
                        }
                    } else {
                        for (j = attacker.y; j < whiteKing.y; j++) {
                            if (threatenedTile(getTile(i, j), colour))
                                return true;
                        }
                    }
                }
            }
        } else{
            if(checkingBlack[0]!=null && checkingBlack[1]!=null)
                return false;
            attacker = checkingBlack[0];
            if(attacker.isKnight)
                return threatenedTile(getTile(attacker), colour);
            if(attacker.x >= blackKing.x){
                for(i=attacker.x; i>blackKing.x; i--) {
                    if (attacker.y >= blackKing.y) {
                        for (j = attacker.y; j > blackKing.y; j--) {
                            if (threatenedTile(getTile(i, j), colour))
                                return true;
                        }
                    } else {
                        for (j = attacker.y; j < blackKing.y; j++) {
                            if (threatenedTile(getTile(i, j), colour))
                                return true;
                        }
                    }
                }
            } else {
                for(i=attacker.x; i<blackKing.x; i++) {
                    if (attacker.y >= blackKing.y) {
                        for (j = attacker.y; j > blackKing.y; j--) {
                            if (threatenedTile(getTile(i, j), colour))
                                return true;
                        }
                    } else {
                        for (j = attacker.y; j < blackKing.y; j++) {
                            if (threatenedTile(getTile(i, j), colour))
                                return true;
                        }
                    }
                }
            }
        }
        return false;
    }
    public void setCheck(boolean black_in_check){
        if(black_in_check)
            blackCheck = true;
        else
            whiteCheck = true;
        System.out.println("Check");
    }
    public boolean threatenedTile(Tile target, boolean whiteAggressor){
        int i, j, count;
        Piece occupant;
        boolean isThreatened = false;
        int threateningKing = 0;
        if(target==getTile(blackKing))
            threateningKing = 2;
        else if(target==getTile(whiteKing))
            threateningKing = 4;
        if(whiteAggressor)
            count = remainingWhite;
        else
            count = remainingBlack;
        if(target.getPiece()!=null){
            if(whiteAggressor == target.getPiece().isWhite)
                COUNTERATTACK_FLAG = true;
        }
        for(i=0; i<8; i++){
            for(j=0; j<8; j++){
                occupant = getTile(i,j).getPiece();
                if(occupant!=null){
                    if(whiteAggressor != occupant.isWhite || occupant==blackKing || occupant==whiteKing)
                        continue;
                    else
                        count--;
                    if(occupant.movePiece(getTile(occupant), target, this, false)){
                        if(threateningKing>0){
                            if(threateningKing<3){
                                checkingBlack[threateningKing%2] = occupant;
                                threateningKing--;
                            } else {
                                checkingWhite[threateningKing%2] = occupant;
                                threateningKing--;
                            }
                        }
                        COUNTERATTACK_FLAG = false;
                        isThreatened = true;
                    }
                }
                if(count==0)
                    return isThreatened;
            }
        }
        return isThreatened;
    }
    private void determineMobility(boolean white){
        int i, j, dx, dy;
        if(white){
            for(i=-1; i<2; i++){
                dx = whiteKing.x + i;
                if(dx<0 || dx>7)
                    continue;;
                for(j=-1; j<2; j++) {
                    dy = whiteKing.y + j;
                    if(dy<0 || dy>7)
                        continue;
                    if (whiteKing.movePiece(getTile(whiteKing), getTile(dx, dy), this, false)){
                        whiteKing_immobile = false;
                        return;
                    }
                }
            }
            whiteKing_immobile = true;
        } else {
            for(i=-1; i<2; i++){
                dx = blackKing.x + i;
                if(dx<0 || dx>7)
                    continue;;
                for(j=-1; j<2; j++) {
                    dy = blackKing.y + j;
                    if(dy<0 || dy>7)
                        continue;
                    if(blackKing.movePiece(getTile(blackKing), getTile(dx, dy), this, false)){
                        blackKing_immobile = false;
                        return;
                    }

                }
            }
            blackKing_immobile = true;
        }
    }
    private String debug_string_helper(String s, Piece p){
        String colour = "";
        if(p.isWhite)
            colour = "white ";
        else
            colour = "black ";
        String piece = "";
        switch(p.getChessPiece().charAt(1)){
            case('p'):
                piece = "pawn";
                break;
            case('R'):
                piece = "Rook ";
                break;
            case('N'):
                piece = "Knight ";
                break;
            case('B'):
                piece = "Bishop ";
                break;
            case('Q'):
                piece = "Queen ";
                break;
            case('K'):
                piece = "King ";
                break;
        }
        return ("(" + s + ") " + colour + " " + piece);
    }
    public void debug_move_string(String start, String dest, Piece sp, Piece dp){
        String half1 = debug_string_helper(start, sp);
        if(dp==null){
            System.out.println(half1 + " to " + "(" + dest + ") ");
            return;
        }
        String half2 = debug_string_helper(dest, dp);
        System.out.println(half1 + " takes " + half2);
    }
}
